import discord
import asyncio
import datetime
import pymysql
import threading
import sys
from warnings import filterwarnings
from datetime import timezone

import config

filterwarnings('ignore', category = pymysql.Warning)

print('Connecting...')
client = discord.Client()

@client.event
async def on_ready():
    for server in client.servers:
        if not (onBlacklist(server.id)):
            print("Initializing data for server: ", (server.name))
            saveServer(server)

            counter = 1
            total = len(server.channels)
            for channel in server.channels:
                print("{}Saving Channel: {}/{}.".format("\033[K", counter, total), end="\r")
                counter += 1
                saveChannel(channel)

            counter = 1
            total = len(server.members)
            for member in server.members:
                print("{}Saving Member: {}/{}.".format("\033[K", counter, total), end="\r")
                counter += 1
                saveMember(member)

            counter = 1
            total = len(server.emojis)
            for emoji in server.emojis:
                print("{}Saving Custom Emoji: {}/{}.".format("\033[K", counter, total), end="\r")
                saveEmoji(emoji)

            counter = 1
            total = len(server.roles)
            for role in server.roles:
                print("{}Saving Role: {}/{}.".format("\033[K", counter, total), end="\r")
                saveRole(role)
                savePermission(role)

    counter = 1
    total = len(client.private_channels)
    for privateChannel in client.private_channels:
        print("{}Saving Private Channel: {}/{}.{}".format("\033[K", counter, total, "\r"), end="\r")
        saveChannel(privateChannel)

    task = asyncio.Task(getInvites())
    loop = asyncio.get_event_loop()
    asyncio.run_coroutine_threadsafe(getInvites(), client.loop)


    print("{}Saving Ban List:".format("\033[K"), end="\r")
    task = asyncio.Task(saveBans())
    asyncio.run_coroutine_threadsafe(saveBans(), client.loop)

    if (config.status != ""):
        await client.change_presence(game=discord.Game(name=config.status))
    else:
        await client.change_presence(game=None)

    # await notifyOwner("I'm Working!", ())

    print('-----------------------------')
    print('Logged in as:')
    print('Username: ' + client.user.name)
    print('Bot ID: ' + client.user.id)
    print('-----------------------------')

@client.event
async def on_message(message):
    if (message.author.id in config.owners and message.channel.is_private):
        if (message.content == '!ping'):
            await client.send_message(message.author, 'Pong!')
        if (message.content == '!stop'):
            exit("Stopped from within Discord by {}({})".format(message.author.name, message.author.id))
    saveMessage(message)

@client.event
async def on_message_edit(before, after):
    # TODO: save when message is edited
    db = pymysql.connect(host=config.db_host, port=3306, user=config.db_user, passwd=config.db_passwd, db=config.db_name)
    cur = db.cursor()
    cur.execute("SELECT `version` from messages where `id` = %s ORDER BY `version` DESC LIMIT 1", (before.id, ))
    version = cur.fetchone()
    if version is not None:
        version = version[0]
        saveMessage(after, version + 1)
    cur.close()
    db.close()

@client.event
async def on_message_delete(message):
    executeQuery("UPDATE messages SET deleted=CURRENT_TIMESTAMP WHERE id = %s;", (message.id, ))

@client.event
async def on_channel_create(channel):
    saveChannel(channel)

@client.event
async def on_channel_delete(channel):
    executeQuery("UPDATE channels SET deleted=CURRENT_TIMESTAMP WHERE id = %s;", (channel.id, ))

@client.event
async def on_channel_update(before, after):
    if (before.name != after.name):
        saveChannelChange(after, "name", str(before.name), str(after.name))
    if (before.topic != after.topic):
        saveChannelChange(after, "topic", str(before.topic), str(after.topic))
    if (before.position != after.position):
        saveChannelChange(after, "position", str(before.position), str(after.position))
    if (before.bitrate != after.bitrate):
        saveChannelChange(after, "bitrate", str(before.bitrate), str(after.bitrate))
    if (before.user_limit != after.user_limit):
        saveChannelChange(after, "user_limit", str(before.user_limit), str(after.user_limit))
    if (before.is_default != after.is_default):
        saveChannelChange(after, "is_default", str(before.is_default), str(after.is_default))
    saveChannel(after)

@client.event
async def on_member_join(member):
    if not (onBlacklist(member.server.id)):
        await notifyOwner("[%s](%s) joined [%s](%s).", (member.name, member.id, member.server.name, member.server.id))
    saveMemberChange(member, "joined", str(member.server.id), "")
    saveMember(member)

@client.event
async def on_member_remove(member):
    if not (onBlacklist(member.server.id)):
        await notifyOwner("[%s](%s) left [%s](%s).", (member.name, member.id, member.server.name, member.server.id))
    saveMemberChange(member, "left", str(member.server.id), "")
    executeQuery("DELETE FROM servers_members WHERE server_id = %s AND member_id = %s;", (member.server.id, member.id, ))

@client.event
async def on_member_update(before, after):
    if (before.status != after.status):
        saveMemberChange(after, "status", str(before.status), str(after.status))
    if (before.game != after.game):
        before_game = "None"
        if before.game is not None:
            before_game = str(before.game.name)
        after_game = "None"
        if after.game is not None:
            after_game = str(after.game.name)
        saveMemberChange(after, "game", before_game, after_game)
    if (before.avatar_url != after.avatar_url):
        saveMemberChange(after, "avatar", str(before.avatar_url), str(after.avatar_url))
    if (before.nick != after.nick):
        saveMemberChange(after, "nick", str(before.nick), str(after.nick))
    if (before.roles != after.roles):
        for role in before.roles:
            if (role not in after.roles):
                if not (onBlacklist(after.server.id)):
                    await notifyOwner("[%s](%s) was taken the role [%s](%s) in server [%s](%s).", (after.name, after.id, role.name, role.id, after.server.name, after.server.id))
                    saveMemberChange(after, "role", str(role.id), "removed")
        for role in after.roles:
            if (role not in before.roles):
                if not (onBlacklist(after.server.id)):
                    await notifyOwner("[%s](%s) was granted the role [%s](%s) in server [%s](%s).", (after.name, after.id, role.name, role.id, after.server.name, after.server.id))
                    saveMemberChange(after, "role", str(role.id), "added")
    saveMember(after)

@client.event
async def on_server_join(server):
    if not (onBlacklist(server.id)):
        await notifyOwner("I joined [%s](%s).", (server.name, server.id))
    saveServerChange(server, "joined", "", "")
    saveServer(server)

@client.event
async def on_server_remove(server):
    if not (onBlacklist(server.id)):
        await notifyOwner("I left [%s](%s).", (server.name, server.id))
    saveServerChange(server, "left", "", "")
    executeQuery("UPDATE servers SET deleted=CURRENT_TIMESTAMP WHERE id = %s;", (server.id, ))

@client.event
async def on_server_update(before, after):
    if (before.name != after.name):
        saveServerChange(after, "name", str(before.name), str(after.name))
    if (before.afk_timeout != after.afk_timeout):
        saveServerChange(after, "afk_timeout", str(before.afk_timeout), str(after.afk_timeout))
    if (before.afk_channel != after.afk_channel):
        before_afk = "None"
        after_afk = "None"
        if before.channel is not None:
            before_afk = before.afk_channel.id
        if after.channel is not None:
            after_afk = after.afk_channel.id
        saveServerChange(after, "afk_channel", str(before_afk), str(after_afk))
    saveServer(after)

@client.event
async def on_member_ban(member):
    if not (onBlacklist(member.server.id)):
        await notifyOwner("[%s](%s) got banned from [%]s(%s).", (member.name, member.id, member.server.name, member.server.id))
    saveBan(member.server.id, member.id)

@client.event
async def on_member_unban(server, member):
    if not (onBlacklist(server.id)):
        await notifyOwner("[%s](%s) got unbanned from [%s](%s).", (member.mention, member.id, server.name, server.id))
    executeQuery("DELETE FROM bans WHERE server_id = %s AND member_id = %s;", (server.id, member.id))

@client.event
async def on_server_role_create(role):
    saveRole(role)
    savePermission(role)

@client.event
async def on_server_role_delete(role):
    executeQuery("UPDATE `roles` SET deleted=CURRENT_TIMESTAMP WHERE id = %s;", (role.id, ))

@client.event
async def on_server_role_update(before, after):
    if (before.name != after.name):
        saveRoleChange(after, "name", str(before.name), str(after.name))
    if (before.permissions.value != after.permissions.value):
        saveRoleChange(after, "permissions", str(before.permissions.value), str(after.permissions.value))
    if (before.color != after.color):
        saveRoleChange(after, "color", str(before.color), str(after.color))
    if (before.hoist != after.hoist):
        saveRoleChange(after, "hoist", str(before.hoist), str(after.hoist))
    if (before.position != after.position):
        saveRoleChange(after, "position", str(before.position), str(after.position))
    if (before.mentionable != after.mentionable):
        saveRoleChange(after, "mentionable", str(before.mentionable), str(after.mentionable))
    saveRole(after)
    savePermission(after)

@client.event
async def on_voice_state_update(before, after):
    if (before.voice.deaf != after.voice.deaf):
        saveVoiceChange(after, "server_deaf", str(before.voice.deaf), str(after.voice.deaf))
    if (before.voice.mute != after.voice.mute):
        saveVoiceChange(after, "server_mute", str(before.voice.mute), str(after.voice.mute))
    if (before.voice.self_deaf != after.voice.self_deaf):
        saveVoiceChange(after, "self_deaf", str(before.voice.self_deaf), str(after.voice.self_deaf))
    if (before.voice.self_mute != after.voice.self_mute):
        saveVoiceChange(after, "self_mute", str(before.voice.self_mute), str(after.voice.self_mute))
    if (before.voice.is_afk != after.voice.is_afk):
        saveVoiceChange(after, "is_afk", str(before.voice.is_afk), str(after.voice.is_afk))
    if (before.voice.is_afk != after.voice.is_afk):
        saveVoiceChange(after, "is_afk", str(before.voice.is_afk), str(after.voice.is_afk))

    before_voice_channel = "None"
    after_voice_channel = "None"
    if hasattr(before.voice.voice_channel, 'id'):
        before_voice_channel = before.voice.voice_channel.id
    if hasattr(after.voice.voice_channel, 'id'):
        after_voice_channel = after.voice.voice_channel.id
    if (before_voice_channel != after_voice_channel):
        saveVoiceChange(after, "voice_channel", before_voice_channel, after_voice_channel)

@client.event
async def on_reaction_add(reaction, user):
    saveReaction(reaction, user)

@client.event
async def on_reaction_remove(reaction, user):
    if hasattr(reaction.emoji, 'id'):
        emoji = reaction.emoji.id
    else:
        emoji = ord(reaction.emoji[0])
    executeQuery("UPDATE `reactions` SET deleted=CURRENT_TIMESTAMP WHERE `emoji_id` = %s AND `member_id` = %s AND `message_id` = %s;", (emoji, user.id, reaction.message.id))

@client.event
async def on_error(event, args):
    if (event != 'on_ready'):
        print("Unexpected Discord error:", event)

def init():
    print('Setting up database...')
    db = pymysql.connect(host=config.db_host, port=config.db_port, user=config.db_user, passwd=config.db_passwd, charset='utf8mb4')
    cur = db.cursor()
    cur.execute("CREATE DATABASE IF NOT EXISTS " +  config.db_name)
    cur.close()
    db.commit()
    db.close()

    setupDatabase()

    print('Initializing...')
    executeQuery("UPDATE `channels` SET deleted=CURRENT_TIMESTAMP;", ())
    executeQuery("UPDATE `servers` SET deleted=CURRENT_TIMESTAMP;", ())
    executeQuery("UPDATE `private_channels` SET deleted=CURRENT_TIMESTAMP;", ())
    executeQuery("UPDATE `roles` SET deleted=CURRENT_TIMESTAMP;", ())
    executeQuery("DELETE FROM `servers_members` WHERE 1;", ())
    executeQuery("DELETE FROM `member_roles` WHERE 1;", ())
    executeQuery("DELETE FROM `private_channels_recipients` WHERE 1;", ())
    executeQuery("DELETE FROM `bans` WHERE 1;", ())


def saveChannel(channel):
        if channel.is_private is True:
            icon = "None"
            if hasattr(channel, 'icon'):
                icon = channel.icon
            executeQuery("REPLACE INTO `private_channels` (`id`, `type`, `icon`, `name`, `icon_url`, `created_at`) VALUES (%s, %s, %s, %s, %s, %s)", (channel.id, str(channel.type), str(icon), str(channel.name), channel.icon_url, channel.created_at))
            for member in channel.recipients:
                executeQuery("REPLACE INTO `private_channels_recipients` (`private_channel_id`, `member_id`) VALUES (%s, %s)", (channel.id, member.id))
        elif channel.server is not None:
            if not (onBlacklist(channel.server.id)):
                server = channel.server.id
                executeQuery("REPLACE INTO `channels` (`id`, `name`, `server`, `type`, `topic`, `is_private`, `position`, `bitrate`, `user_limit`, `is_default`, `created_at`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (channel.id, channel.name, server, str(channel.type), channel.topic, channel.is_private, channel.position, channel.bitrate, channel.user_limit, channel.is_default, channel.created_at))
        else:
            pass

def saveMember(member):
    if not (onBlacklist(member.server.id)):
        executeQuery("REPLACE INTO `members` (`id`, `name`, `discriminator`, `avatar`, `bot`, `avatar_url`, `default_avatar`, `default_avatar_url`, `mention`, `created_at`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);", (member.id, member.name, member.discriminator, member.avatar, member.bot, member.avatar_url, str(member.default_avatar), member.default_avatar_url,  member.mention, member.created_at))
        executeQuery("REPLACE INTO `servers_members` (`server_id`, `member_id`, `joined_at`, `display_name`) VALUES (%s, %s, %s, %s);", (member.server.id, member.id, convertTimezone(member.joined_at), member.display_name))
        for role in member.roles:
            executeQuery("REPLACE INTO `member_roles` (`member_id`, `role_id`) VALUES (%s, %s);", (member.id, role.id))

def saveMessage(message, version = 1):
    channel = "None"
    server = "None"
    if message.channel is not None:
        channel = message.channel.id
    if message.server is not None:
        server = message.server.id
    if not (onBlacklist(server)):
        executeQuery("REPLACE INTO `messages` (`id`, `content`, `timestamp`, `tts`, `type`, `author`, `channel`, `server`, `mention_everyone`, `version`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);", (message.id, message.content, convertTimezone(message.timestamp), message.tts, str(message.type), message.author.id, channel, server, message.mention_everyone, version))

def saveServer(server):
    if server is not None and not (onBlacklist(server.id)):
        afk_channel = "None"
        splash = "None"
        splash_url = "None"
        if server.afk_channel is not None:
            afk_channel = server.afk_channel.id
        if hasattr(server, 'splash'):
            splash = server.splash
        if hasattr(server, 'splash_url'):
            splash_url = server.splash_url
        executeQuery("REPLACE INTO `servers` (`id`, `name`, `region`, `owner`, `afk_timeout`, `afk_channel`, `icon`, `mfa_level`, `verification_level`, `splash`, `default_channel`, `icon_url`, `splash_url`, `member_count`, `created_at`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (server.id, server.name, str(server.region), server.owner.id, server.afk_timeout, afk_channel, server.icon, server.mfa_level, str(server.verification_level), splash, server.default_channel.id, server.icon_url, splash_url, server.member_count, convertTimezone(server.created_at)))

def saveInvite(invite):
    if not (onBlacklist(invite.server.id)):
        executeQuery("REPLACE INTO `invites`(`id`, `inviter`, `server`, `channel`, `max_age`, `code`, `revoked`, `created_at`, `temporary`, `uses`, `max_uses`, `xkcd`, `url`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (invite.id, invite.inviter.id, invite.server.id, invite.channel.id, invite.max_age, invite.code, invite.revoked, convertTimezone(invite.created_at), invite.temporary, invite.uses, invite.max_uses, invite.xkcd, invite.url))

def saveRole(role):
    if not (onBlacklist(role.server.id)):
        executeQuery("REPLACE INTO `roles`(`id`, `name`, `server`, `color`, `hoist`, `position`, `managed`, `mentionable`,  `created_at`, `permissions`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (role.id, role.name, role.server.id, role.color.value, role.hoist, role.position, role.managed, role.mentionable, convertTimezone(role.created_at), role.permissions.value))

def savePermission(role):
    if not (onBlacklist(role.server.id)):
        permission = role.permissions
        executeQuery("REPLACE INTO `permissions`(`value`, `create_instant_invite`, `kick_members`, `ban_members`, `administrator`, `manage_channels`, `manage_server`, `add_reactions`, `read_messages`, `send_messages`, `send_tts_messages`, `manage_messages`, `embed_links`, `attach_files`, `read_message_history`, `mention_everyone`, `external_emojis`, `connect`, `speak`, `mute_members`, `deafen_members`, `move_members`, `use_voice_activation`, `change_nickname`, `manage_nicknames`, `manage_roles`, `manage_webhooks`, `manage_emojis`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (permission.value, permission.create_instant_invite, permission.kick_members, permission.ban_members, permission.administrator, permission.manage_channels, permission.manage_server, permission.add_reactions, permission.read_messages, permission.send_messages, permission.send_tts_messages, permission.manage_messages, permission.embed_links, permission.attach_files, permission.read_message_history, permission.mention_everyone, permission.external_emojis, permission.connect, permission.speak, permission.mute_members, permission.deafen_members, permission.move_members, permission.use_voice_activation, permission.change_nickname, permission.manage_nicknames, permission.manage_roles, permission.manage_webhooks, permission.manage_emojis))

def saveEmoji(emoji):
    if not (onBlacklist(emoji.server.id)):
        executeQuery("REPLACE INTO `emojis`(`id`, `name`, `require_colons`, `managed`, `server`,  `created_at`, `url`) VALUES (%s, %s, %s, %s, %s, %s, %s)", (emoji.id, emoji.name, emoji.require_colons, emoji.managed, emoji.server.id,  convertTimezone(emoji.created_at), emoji.url))
        for role in emoji.roles:
            executeQuery("REPLACE INTO `emoji_roles` (`emoji_id`, `role_id`) VALUES (%s, %s);", (emoji.id, role.id))

def saveReaction(reaction, member):
    if not (onBlacklist(member.server.id)):
        if hasattr(reaction.emoji, 'id'):
            emoji = reaction.emoji.id
        else:
            emoji = ord(reaction.emoji[0])
        executeQuery("REPLACE INTO `reactions`(`emoji_id`, `member_id`, `message_id`) VALUES (%s, %s, %s)", (emoji, member.id, reaction.message.id))

def saveBan(server_id, member_id):
    if not (onBlacklist(server_id)):
        executeQuery("REPLACE INTO `bans` (`server_id`, `member_id`) VALUES (%s, %s);", (server_id, member_id))


@client.async_event
async def getInvites():
    while True:
        try:
            for server in list(client.servers):
                try:
                    invites = await client.invites_from(server)
                    for invite in invites:
                        saveInvite(invite)
                except discord.Forbidden:
                    pass
        except (KeyboardInterrupt, SystemExit, discord.HTTPException, asyncio.CancelledError, GeneratorExit):
            pass
        except:
            print("Unexpected Invite error:", sys.exc_info()[0])
        await asyncio.sleep(config.invite_timeout)

@client.async_event
async def saveBans():
    try:
        for server in list(client.servers):
            try:
                for member in await client.get_bans(server):
                    saveBan(server.id, member.id)
            except discord.Forbidden:
                pass
    except (KeyboardInterrupt, SystemExit, discord.HTTPException, asyncio.CancelledError):
            pass
    except:
        print("Unexpected Ban error:", sys.exc_info()[0])

def saveMemberChange(member, attribute, before, after):
    if not (onBlacklist(member.server.id)):
        executeQuery("INSERT IGNORE INTO `member_changes` (`member_id`, `server_id`, `attribute`, `before`, `after`) VALUES (%s, %s, %s, %s, %s);", (member.id, member.server.id, attribute, before, after))

def saveServerChange(server, attribute, before, after):
    if not (onBlacklist(server.id)):
        executeQuery("INSERT IGNORE INTO `server_changes` (`server_id`, `attribute`, `before`, `after`) VALUES (%s, %s, %s, %s);", (server.id, attribute, before, after))

def saveChannelChange(channel, attribute, before, after):
    if not (onBlacklist(channel.server.id)):
        executeQuery("INSERT IGNORE INTO `channel_changes` (`channel_id`, `attribute`, `before`, `after`) VALUES (%s, %s, %s, %s);", (channel.id, attribute, before, after))

def saveRoleChange(role, attribute, before, after):
    if not (onBlacklist(role.server.id)):
        executeQuery("INSERT IGNORE INTO `role_changes` (`role_id`, `attribute`, `before`, `after`) VALUES (%s, %s, %s, %s);", (role.id, attribute, before, after))

def saveVoiceChange(member, attribute, before, after):
    if not (onBlacklist(member.server.id)):
        executeQuery("INSERT IGNORE INTO `voice_changes` (`member_id`, `attribute`, `before`, `after`) VALUES (%s, %s, %s, %s);", (member.id, attribute, before, after))

def setupDatabase():
    executeQuery("CREATE TABLE IF NOT EXISTS `channels` (`id` VARCHAR(32) NOT NULL, `name` VARCHAR(512) NOT NULL, `server` VARCHAR(32) NOT NULL, `type` VARCHAR(8) NOT NULL, `topic` VARCHAR(512), `is_private` BOOL, `position` INT, `bitrate` INT, `user_limit` INT, `is_default` BOOL, `created_at` TIMESTAMP NULL DEFAULT NULL , `deleted` TIMESTAMP NULL DEFAULT NULL, PRIMARY KEY (`id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `members` (`id` VARCHAR(32) NOT NULL, `name` VARCHAR(512) NOT NULL, `discriminator` VARCHAR(128) NOT NULL, `avatar` VARCHAR(128), `bot` BOOL, `avatar_url` VARCHAR(512), default_avatar VARCHAR(512), default_avatar_url VARCHAR(512), mention VARCHAR(512), created_at DATETIME, PRIMARY KEY (`id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `messages` (`id` VARCHAR(32) NOT NULL, `content` VARCHAR(2000) NOT NULL, `timestamp` DATETIME, `tts` BOOL, `type` VARCHAR(128), `author` VARCHAR(32), `channel` VARCHAR(32), `server` VARCHAR(32), `mention_everyone` BOOL, `version` INT DEFAULT 1, `deleted` TIMESTAMP NULL DEFAULT NULL, PRIMARY KEY (`id`, `version`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `servers`(`id` VARCHAR(32) NOT NULL, `name` VARCHAR(128) NOT NULL, `region` VARCHAR(128), `owner` VARCHAR(32), `afk_timeout` INT, `afk_channel` VARCHAR(128), `icon` VARCHAR(128), `mfa_level` VARCHAR(128), `verification_level` VARCHAR(128), `splash` VARCHAR(128), `default_channel` VARCHAR(32), `icon_url` VARCHAR(128), `splash_url` VARCHAR(128), `member_count` VARCHAR(32), `created_at` TIMESTAMP NOT NULL, `deleted` TIMESTAMP NULL DEFAULT NULL, PRIMARY KEY (`id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `servers_members`(`server_id` VARCHAR(32) NOT NULL, `member_id` VARCHAR(32) NOT NULL, `joined_at` TIMESTAMP, display_name VARCHAR(512), PRIMARY KEY (`server_id`, `member_id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `member_changes`(`id` INT NOT NULL AUTO_INCREMENT, `member_id` VARCHAR(32) NOT NULL, `server_id` VARCHAR(32) NOT NULL, `timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, `attribute` VARCHAR(32) NOT NULL, `before` VARCHAR(512), `after` VARCHAR(512), PRIMARY KEY (`id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `invites`(`id` VARCHAR(32)  NOT NULL, `inviter` VARCHAR(32) NOT NULL, `server` VARCHAR(32) NOT NULL, `channel` VARCHAR(32), `max_age` INT, `code` VARCHAR(128), `revoked` BOOL,  `created_at` TIMESTAMP NULL DEFAULT NULL, `temporary` BOOL, `uses` INT, `max_uses` INT, `xkcd` VARCHAR(512), `url` VARCHAR(32), PRIMARY KEY (`id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `roles`(`id` VARCHAR(32)  NOT NULL, `name` VARCHAR(128) NOT NULL, `server` VARCHAR(32) NOT NULL, `color` INT, `hoist` BOOL, `position` INT, `managed` BOOL, `mentionable` BOOL,  `created_at` TIMESTAMP NULL DEFAULT NULL,`permissions` INT, `deleted` TIMESTAMP NULL DEFAULT NULL, PRIMARY KEY (`id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `permissions`(`value` INT NOT NULL, `create_instant_invite` BOOL, `kick_members` BOOL, `ban_members` BOOL, `administrator` BOOL, `manage_channels` BOOL, `manage_server` BOOL, `add_reactions` BOOL, `read_messages` BOOL, `send_messages` BOOL, `send_tts_messages` BOOL, `manage_messages` BOOL, `embed_links` BOOL, `attach_files` BOOL, `read_message_history` BOOL, `mention_everyone` BOOL, `external_emojis` BOOL, `connect` BOOL, `speak` BOOL, `mute_members` BOOL, `deafen_members` BOOL, `move_members` BOOL, `use_voice_activation` BOOL, `change_nickname` BOOL, `manage_nicknames` BOOL, `manage_roles` BOOL, `manage_webhooks` BOOL, `manage_emojis` BOOL, PRIMARY KEY (`value`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `member_roles`(`member_id` VARCHAR(32) NOT NULL, `role_id` VARCHAR(32), PRIMARY KEY (`member_id`, `role_id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `emojis`(`id` VARCHAR(32)  NOT NULL, `name` VARCHAR(128) NOT NULL, `require_colons` BOOL, `managed` BOOL, `server` VARCHAR(32) NOT NULL,  `created_at` TIMESTAMP NULL DEFAULT NULL, `url` VARCHAR(512), PRIMARY KEY (`id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `emoji_roles`(`emoji_id` VARCHAR(32) NOT NULL, `role_id` VARCHAR(32), PRIMARY KEY (`emoji_id`, `role_id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `server_changes`(`id` INT NOT NULL AUTO_INCREMENT, `server_id` VARCHAR(32) NOT NULL, `timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, `attribute` VARCHAR(32) NOT NULL, `before` VARCHAR(512), `after` VARCHAR(512), PRIMARY KEY (`id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `channel_changes`(`id` INT NOT NULL AUTO_INCREMENT, `channel_id` VARCHAR(32) NOT NULL, `timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, `attribute` VARCHAR(32) NOT NULL, `before` VARCHAR(512), `after` VARCHAR(512), PRIMARY KEY (`id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `role_changes`(`id` INT NOT NULL AUTO_INCREMENT, `role_id` VARCHAR(32) NOT NULL, `timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, `attribute` VARCHAR(32) NOT NULL, `before` VARCHAR(512), `after` VARCHAR(512), PRIMARY KEY (`id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `voice_changes`(`id` INT NOT NULL AUTO_INCREMENT, `member_id` VARCHAR(32) NOT NULL, `timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, `attribute` VARCHAR(32) NOT NULL, `before` VARCHAR(512), `after` VARCHAR(512), PRIMARY KEY (`id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `reactions`(`id` INT NOT NULL AUTO_INCREMENT, `emoji_id` VARCHAR(32)  NOT NULL, `member_id` VARCHAR(32) NOT NULL, `message_id` VARCHAR(32) NOT NULL, `timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, `deleted` TIMESTAMP NULL DEFAULT NULL, PRIMARY KEY (`id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `private_channels` (`id` VARCHAR(32) NOT NULL, `type` VARCHAR(32) NOT NULL, `icon` VARCHAR(512) NOT NULL, `name` VARCHAR(128) NOT NULL, `icon_url` VARCHAR(512), `created_at` TIMESTAMP NULL DEFAULT NULL, `deleted` TIMESTAMP NULL DEFAULT NULL, PRIMARY KEY (`id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `private_channels_recipients` (`private_channel_id` VARCHAR(32) NOT NULL, `member_id` VARCHAR(32) NOT NULL, PRIMARY KEY (`private_channel_id`, `member_id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())
    executeQuery("CREATE TABLE IF NOT EXISTS `bans` (`server_id` VARCHAR(32) NOT NULL, `member_id` VARCHAR(32) NOT NULL, PRIMARY KEY (`server_id`, `member_id`) ) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;", ())

def executeQuery(query, data):
    try:
        db = pymysql.connect(host=config.db_host, port=config.db_port, user=config.db_user, passwd=config.db_passwd, db=config.db_name, charset='utf8mb4')
        cur = db.cursor()
        cur.execute(query, data)
        cur.close()
        db.commit()
        db.close()
    except (asyncio.CancelledError):
        pass
    except (KeyboardInterrupt):
        loop = asyncio.get_event_loop()
        future = asyncio.Future()
        loop.run_until_complete(future)
        loop.close()
    except:
        print("Unexpected MySQL error:", sys.exc_info()[0])


def onBlacklist(server_id):
    if (server_id in config.blacklist):
        return True
    return False

def convertTimezone(discordTime):
    return discordTime.replace(tzinfo=timezone.utc).astimezone(tz=None)
async def notifyOwner(message, data):
    for owner_id in config.owners:
        try:
            owner = await client.get_user_info(owner_id)
            if owner is not None:
                await client.send_message(owner, '```css\n' + message % (data) + '```')
        except discord.HTTPException:
            print ("Unknown User: %s in owners list. Check your config.py" % (owner_id))

init()
client.run(config.token)
